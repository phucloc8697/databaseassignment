import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import database.DatabaseHelper;

public class ManageBill extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton backButton;
	private JComboBox<String> comboBox;
	private JTable table;
		
	private int currentId;
	private String role;
	private int branch;
	
	public ManageBill(int id) {
		this.currentId = id;
		this.role = DatabaseHelper.shared.getRole(id);
		this.branch = DatabaseHelper.staff.getBranch(id);
		try {
			initialize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initialize() throws Exception {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(DatabaseHelper.bill.getOptionsComboBox(this.role, this.branch)));
		comboBox.setBounds(10, 11, 190, 38);
		comboBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				try {
					updateTable();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(comboBox);
		
		backButton = new JButton("Quay l\u1EA1i");
		backButton.setBounds(10, 362, 190, 38);
		backButton.setActionCommand("Back");
		backButton.addActionListener(this);
		contentPane.add(backButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(210, 11, 764, 389);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setEnabled(false);
		table.setBounds(314, 11, 660, 389);
		scrollPane.setViewportView(table);
		
		updateTable();
	}
	
	private void updateTable() throws Exception {
		String[] titles = DatabaseHelper.bill.getTitles((String) comboBox.getSelectedItem(), this.branch);
		String[][] data = DatabaseHelper.bill.getData((String) comboBox.getSelectedItem(), this.branch);
		table.setModel(new DefaultTableModel(data, titles));
	}
	
	
	private void back() {
		Menu frame = new Menu(this.currentId);
		frame.setVisible(true);
		dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Back":
			back();
			break;
		default:
			break;
		}
	}
}