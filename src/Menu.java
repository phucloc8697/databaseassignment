import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import database.DatabaseHelper;
import utility.Constant;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Menu extends JFrame implements ActionListener {

	private JPanel contentPane;
	
	private JButton staffButton;
	private JButton itemButton;
	private JButton activityButton;
	private JButton sellButton;
	private JButton billButton;
	private JButton signoutButton;

	private String role;
	private int currentId;
//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Menu frame = new Menu();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * Create the frame.
//	 */
	public Menu(int id) {
		try {
			initialize(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void initialize(int id) throws Exception {
		this.currentId = id;
		this.role = DatabaseHelper.shared.getRole(id);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		if (!this.role.equals(Constant.ROLL_STAFF)) {
			staffButton = new JButton("Qu\u1EA3n l\u00ED nh\u00E2n vi\u00EAn");
			staffButton.setActionCommand("Staff");
			staffButton.addActionListener(this);
			staffButton.setBounds(10, 11, 264, 37);
			contentPane.add(staffButton);
			
			billButton = new JButton("Qu\u1EA3n l\u00ED h\u00F3a \u0111\u01A1n");
			billButton.setActionCommand("Bill");
			billButton.addActionListener(this);
			billButton.setBounds(10, 155, 264, 37);
			contentPane.add(billButton);
			
			sellButton = new JButton("B\u00E1n h\u00E0ng");
			sellButton.setActionCommand("Sell");
			sellButton.addActionListener(this);
			sellButton.setBounds(10, 203, 264, 37);
			contentPane.add(sellButton);
		}
		
		itemButton = new JButton("Qu\u1EA3n l\u00ED h\u00E0ng h\u00F3a");
		itemButton.setActionCommand("Item");
		itemButton.addActionListener(this);
		itemButton.setBounds(10, 59, 264, 37);
		contentPane.add(itemButton);
		
		activityButton = new JButton("Qu\u1EA3n l\u00ED ho\u1EA1t \u0111\u1ED9ng");
		activityButton.setActionCommand("Activity");
		activityButton.addActionListener(this);
		activityButton.setBounds(10, 107, 264, 37);
		contentPane.add(activityButton);
		
		signoutButton = new JButton("\u0110\u0103ng xu\u1EA5t");
		signoutButton.setActionCommand("Sign out");
		signoutButton.addActionListener(this);
		signoutButton.setBounds(10, 313, 264, 37);
		contentPane.add(signoutButton);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Staff":
			openStaff();
			break;
		case "Item":
			openItem();
			break;
		case "Activity":
			openActivity();
			break;
		case "Bill":
			openBill();
			break;
		case "Sell":
			openSell();
			break;
		case "Sign out":
			signout();
			break;
		default:
			break;
		}
	}
	
	private void openStaff() {
		ManageStaff frame = new ManageStaff(this.currentId);
		frame.setVisible(true);
		dispose();
	}
	
	private void openActivity() {
		ManageActivity frame = new ManageActivity(this.currentId);
		frame.setVisible(true);
		dispose();
	}
	
	private void openItem() {
		ManageItem frame = new ManageItem(this.currentId);
		frame.setVisible(true);
		dispose();
	}
	
	private void openBill() {
		ManageBill frame = new ManageBill(this.currentId);
		frame.setVisible(true);
		dispose();
	}
	
	private void openSell() {
		try {
			SellDialog dialog = new SellDialog(this, true, this.currentId);
			dialog.addListener(new SellDialog.Listener() {
				
				@Override
				public void onReturnValue(Object obj) {
					
				}
			});
			dialog.setVisible(true);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void signout() {
		Login frame = new Login();
		frame.setVisible(true);
		dispose();
	}
}
