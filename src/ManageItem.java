import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import database.DatabaseHelper;
import utility.Constant;

public class ManageItem extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton backButton;
	private JButton muaXeButton;
	private JComboBox<String> comboBox;
	private JTable table;
		
	private int currentId;
	private String role;
	private int branch;
	
	private Map<String, String> tableNamesMap;
	
	public ManageItem(int id) {
		this.currentId = id;
		this.role = DatabaseHelper.shared.getRole(id);
		this.branch = DatabaseHelper.staff.getBranch(id);
		initTableNamesMap();
		try {
			initialize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initTableNamesMap() {
		tableNamesMap = new HashMap<>();
		tableNamesMap.put("Lo thuc an", Constant.TABLE_LO_THUC_AN);
		tableNamesMap.put("Lo tom", Constant.TABLE_LO_TOM);
		tableNamesMap.put("Thuoc", Constant.TABLE_THUOC);
		tableNamesMap.put("Vat tu", Constant.TABLE_VAT_TU);
		tableNamesMap.put("Xe", Constant.TABLE_XE);
	}

	private void initialize() throws Exception {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBox = new JComboBox<String>();
		String[] options = tableNamesMap.keySet().toArray(new String[tableNamesMap.size()]);
		comboBox.setModel(new DefaultComboBoxModel<String>(options));

		comboBox.setBounds(10, 11, 190, 38);
		comboBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				try {
					updateTable();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(comboBox);
		
		backButton = new JButton("Quay l\u1EA1i");
		backButton.setBounds(10, 362, 190, 38);
		backButton.setActionCommand("Back");
		backButton.addActionListener(this);
		contentPane.add(backButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(210, 11, 764, 389);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setEnabled(false);
		table.setBounds(314, 11, 660, 389);
		scrollPane.setViewportView(table);
		
		if (!this.role.equals(Constant.ROLL_STAFF)) {
			muaXeButton = new JButton("Mua xe");
			muaXeButton.setActionCommand("Mua xe");
			muaXeButton.addActionListener(this);
			muaXeButton.setBounds(10, 60, 190, 38);
			contentPane.add(muaXeButton);
		}
			
		updateTable();
	}
	
	private void updateTable() throws Exception {
		String name = tableNamesMap.get((String) comboBox.getSelectedItem());
		String[] titles = DatabaseHelper.shared.getTitles(name);
		if (this.branch == 1 && this.role.equals(Constant.ROLL_BOSS)) {
			String[][] data = DatabaseHelper.shared.getData(name);
			table.setModel(new DefaultTableModel(data, titles));
		} else {
			String[][] data = DatabaseHelper.shared.getData(name, this.branch);
			table.setModel(new DefaultTableModel(data, titles));	
		}
	}
	
	private void buy() {
		try {
			BuyDialog dialog = new BuyDialog(this, true, this.currentId);
			dialog.addListener(new BuyDialog.Listener() {
				
				@Override
				public void onReturnValue(Object obj) {
					try {
						updateTable();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			dialog.setVisible(true);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void back() {
		Menu frame = new Menu(this.currentId);
		frame.setVisible(true);
		dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Mua xe":
			buy();
			break;
		case "Back":
			back();
			break;
		default:
			break;
		}
	}
}