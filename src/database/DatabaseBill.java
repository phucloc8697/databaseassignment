package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import utility.Constant;

public class DatabaseBill extends DatabaseHelper {
	
	private String[] viewMua = {Constant.VIEW_HOA_DON_MUA_1, Constant.VIEW_HOA_DON_MUA_2, Constant.VIEW_HOA_DON_MUA_3};
	private String[] viewBan = {Constant.VIEW_HOA_DON_BAN_1, Constant.VIEW_HOA_DON_BAN_2, Constant.VIEW_HOA_DON_BAN_3};

	public String[] getOptionsComboBox(String role, int branch) {
		String[] array = {
				"Hoa don mua",
				"Hoa don ban"
		};
		return array;
	}
	
	public String[] getTitlesForSell() throws Exception {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM hoa_don_ban1");
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int count = metaData.getColumnCount(); //number of column
			String columnName[] = new String[count];

			for (int i = 2; i <= count; i++) {
			   columnName[i-1] = metaData.getColumnLabel(i);
			}
			return columnName;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public String[] getTitles(String selected, int branch) throws Exception {
		try {
			String table = "";
			if (selected.equals("Hoa don mua")) {
				table = viewMua[branch-1];
			} else {
				table = viewBan[branch-1];
			}
			
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + table);
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int count = metaData.getColumnCount(); //number of column
			String columnName[] = new String[count];

			for (int i = 1; i <= count; i++) {
			   columnName[i-1] = metaData.getColumnLabel(i);
			}
			return columnName;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public String[][] getData(String selected, int branch) throws Exception {
		try {
			Connection connection = getConnection();
			
			String table = "";
			if (selected.equals("Hoa don mua")) {
				table = viewMua[branch-1];
			} else {
				table = viewBan[branch-1];
			}
			String sql = "SELECT * FROM " + table;
			System.out.println(sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int columnCount = metaData.getColumnCount(); //number of column
			ArrayList<String[]> arrayList = new ArrayList<String[]>();
			while(result.next()) {
				String[] row = new String[columnCount];
				for (int i = 1; i <= columnCount; i++) {
					row[i-1] = String.valueOf(result.getObject(i));
				}
				arrayList.add(row);
			}
			String[][] array2d = new String[arrayList.size()][columnCount];
			for (int i = 0; i < arrayList.size(); i++) {
				array2d[i] = arrayList.get(i);
			}
			return array2d;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public void insertSell(String[] values, int userId) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			// Create Hoa_don
			String sql = "INSERT INTO " + DATABASE_NAME + "." + Constant.TABLE_HOA_DON + " VALUES (";
			for (int i = 0; i < 5; i++) {
				if (i == 4) {
					sql += "'" + values[i] + "');"; 
				} else if (i == 0) {
					sql += "null,";
				} else {
					sql += "'" + values[i] + "',";
				}
			}
			System.out.println(sql);
			statement.executeUpdate(sql);
			
			// Get MAX Id 
			String sqlMax = "SELECT MAX(Id) from " + Constant.TABLE_HOA_DON;
			ResultSet result = statement.executeQuery(sqlMax);
			int id = 0;
			if (result.next()){
			   id = result.getInt(1);  				
			}
			
			// Create Hoa_don_ban
			String sqlSell = "INSERT INTO " + DATABASE_NAME + "." + Constant.TABLE_HOA_DON_BAN + " VALUES (" + id + ",";
			for (int i = 5; i < values.length; i++) {
				if (i == values.length - 1) {
					sqlSell += "'" + values[i] + "');"; 
				} else if (i == values.length - 2) {
					sqlSell += "'" + String.valueOf(userId) + "',";
				} else {
					sqlSell += "'" + values[i] + "',";
				}
			}
			System.out.println(sqlSell);
			statement.executeUpdate(sqlSell);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void insertMuaXe(String[] xeValues, String[] billValues, int userId) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			
			// Create Hoa_don
			String sqlHoadon = "INSERT INTO " + DATABASE_NAME + "." + Constant.TABLE_HOA_DON + " VALUES (null,";
			for (int i = 0; i < billValues.length - 1; i++) {
				if (i == billValues.length - 2) {
					sqlHoadon += "'" + billValues[i] + "');"; 
				} else {
					sqlHoadon += "'" + billValues[i] + "',";
				}
			}
			System.out.println(sqlHoadon);
			statement.executeUpdate(sqlHoadon);
			
			// Get MAX Id 
			String sqlMax = "SELECT MAX(Id) from " + Constant.TABLE_HOA_DON;
			ResultSet result = statement.executeQuery(sqlMax);
			int idHoadon = 0;
			if (result.next()){
				idHoadon = result.getInt(1);  				
			}
			
			// Create Hoa_don_mua
			String sqlHoadonmua = "INSERT INTO " + DATABASE_NAME + "." + Constant.TABLE_HOA_DON_MUA + " VALUES (";
			sqlHoadonmua += String.valueOf(idHoadon) + ",";
			sqlHoadonmua += String.valueOf(userId) + ",";
			sqlHoadonmua += billValues[billValues.length-1] + ");";

			System.out.println(sqlHoadonmua);
			statement.executeUpdate(sqlHoadonmua);
						
			// Create xe
			String sqlXe = "INSERT INTO " + DATABASE_NAME + "." + Constant.TABLE_XE + " VALUES (null,";
			for (int i = 0; i < xeValues.length; i++) {
				sqlXe += "'" + xeValues[i] + "',";
			}
			sqlXe += String.valueOf(idHoadon) + ");";
			System.out.println(sqlXe);
			statement.executeUpdate(sqlXe);
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
