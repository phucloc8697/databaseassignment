package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import utility.Constant;

public class DatabaseStaff extends DatabaseHelper {

	public int getBranch(int id) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + Constant.TABLE_NHAN_VIEN + " WHERE Id=" + String.valueOf(id));
			
			ResultSet result = statement.executeQuery();
			while(result.next()) {
				return result.getInt("Chi_nhanh");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return 0;
	}
	
	public String[] getTitles(int branch) {
		try {
			Connection connection = getConnection();
			String table = Constant.VIEW_NHAN_VIEN_1;
			if (branch == 2) {
				table = Constant.VIEW_NHAN_VIEN_2;
			} else if (branch == 3) {
				table = Constant.VIEW_NHAN_VIEN_3;
			}
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + table);
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int count = metaData.getColumnCount(); //number of column
			String columnName[] = new String[count];

			for (int i = 1; i <= count; i++) {
			   columnName[i-1] = metaData.getColumnLabel(i);
			}
			return columnName;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public String[][] getData(int branch) throws Exception {
		try {
			Connection connection = getConnection();
			String table = Constant.VIEW_NHAN_VIEN_1;
			if (branch == 2) {
				table = Constant.VIEW_NHAN_VIEN_2;
			} else if (branch == 3) {
				table = Constant.VIEW_NHAN_VIEN_3;
			}
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + table);
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int columnCount = metaData.getColumnCount(); //number of column
			ArrayList<String[]> arrayList = new ArrayList<String[]>();
			while(result.next()) {
				String[] row = new String[columnCount];
				for (int i = 1; i <= columnCount; i++) {
					row[i-1] = String.valueOf(result.getObject(i));
				}
				arrayList.add(row);
			}
			String[][] array2d = new String[arrayList.size()][columnCount];
			for (int i = 0; i < arrayList.size(); i++) {
				array2d[i] = arrayList.get(i);
			}
			return array2d;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public void insert(String[] values) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			String sql = "INSERT INTO " + DATABASE_NAME + "." + Constant.TABLE_NHAN_VIEN + " VALUES (";
			for (int i = 0; i < values.length - 1; i++) {
				if (i == values.length - 2) {
					sql += "'" + values[i] + "');"; 
				} else if (i == 0) {
					sql += "null,";
				} else {
					sql += "'" + values[i] + "',";
				}
			}
			System.out.println(sql);
			statement.executeUpdate(sql);
			if (!values[values.length - 1].isEmpty()) {
				createLaiXe(values[values.length - 1]);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void update(String[] titles, String[] values) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			String sql = "UPDATE " + DATABASE_NAME + "." + Constant.TABLE_NHAN_VIEN + " SET ";
			for (int i = 1; i < values.length - 1; i++) {
				if (i == titles.length - 2) {
					sql += titles[i] + "=" + "'" + values[i] + "' "; 
				} else {
					sql += titles[i] + "=" + "'" + values[i] + "', "; 
				}
			}
			sql += "WHERE Id=" + "'" + values[0] + "'";
			System.out.println(sql);
			statement.executeUpdate(sql);
			if (!values[values.length - 1].isEmpty()) {
				updateLaiXe(values[0], values[values.length - 1]);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void createLaiXe(String num) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			String sql = "SELECT MAX(Id) from " + Constant.TABLE_NHAN_VIEN;
			ResultSet result = statement.executeQuery(sql);
			int id = 0;
			if (result.next()){
			   id = result.getInt(1);  				
			   connection.createStatement().executeUpdate("INSERT INTO " + DATABASE_NAME + "." + Constant.TABLE_LAI_XE 
						+ " VALUES ('" + id + "','" + num + "');");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void updateLaiXe(String id, String num) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			String sql = "UPDATE " + DATABASE_NAME + "." + Constant.TABLE_LAI_XE + " SET Giay_phep_so='" + num + "' WHERE Id=" + id;
			statement.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	
}
