package database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;

import utility.Constant;

public class DatabaseHelper {
	
	public static final String DATABASE_NAME = "assdbs";
	public static final String DATABASE_URL = "jdbc:mysql://localhost:3306/" + DATABASE_NAME;
	public static final String DATABASE_USERNAME = "root";
	public static final String DATABASE_PASSWORD = "0944281788";

	public static DatabaseHelper shared = new DatabaseHelper();
	public static DatabaseStaff staff = new DatabaseStaff();
	public static DatabaseActivity activity = new DatabaseActivity();
	public static DatabaseBill bill = new DatabaseBill();

	public Connection getConnection() throws Exception {
		try {
			String driver = "com.mysql.cj.jdbc.Driver";
			Class.forName(driver);
			
			Connection connection = DriverManager.getConnection(DATABASE_URL, DATABASE_USERNAME, DATABASE_PASSWORD);
			System.out.println("Connect to database successfully");
			return connection;
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return null;
	}
	
	public int authenticate(String username, String password) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + Constant.TABLE_NGUOI_DUNG);
			
			ResultSet result = statement.executeQuery();
			while(result.next()) {
				if (username.equals(result.getObject("Username")) && password.equals(result.getObject("Password"))) {
					return (int) result.getObject("Id");
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return 0;
	}
	
	public String getRole(int id) {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + Constant.TABLE_NGUOI_DUNG + " WHERE Id=" + String.valueOf(id));
			
			ResultSet result = statement.executeQuery();
			while(result.next()) {
				return result.getString("Role");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public String[] getAllTableNames(String role) throws Exception {
		try {
			Connection connection = getConnection();

			DatabaseMetaData metaData = connection.getMetaData();
			ArrayList<String> names = new ArrayList<String>();
			String[] types = {"TABLE"};
			ResultSet rs = metaData.getTables(DATABASE_NAME, null, "%", types);
            while (rs.next()) {
            	if (role.equals(Constant.ROLL_STAFF) && rs.getString("Table_NAME").equals(Constant.TABLE_NGUOI_DUNG))
            		continue;
                names.add(rs.getString("TABLE_NAME"));
            }
			return names.stream().toArray(String[]::new);
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public String[] getTitles(String table) throws Exception {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + table);
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int count = metaData.getColumnCount(); //number of column
			String columnName[] = new String[count];

			for (int i = 1; i <= count; i++) {
			   columnName[i-1] = metaData.getColumnLabel(i);
			}
			return columnName;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public String[][] getData(String table) throws Exception {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + table);
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int columnCount = metaData.getColumnCount(); //number of column
			ArrayList<String[]> arrayList = new ArrayList<String[]>();
			while(result.next()) {
				String[] row = new String[columnCount];
				for (int i = 1; i <= columnCount; i++) {
					row[i-1] = String.valueOf(result.getObject(i));
				}
				arrayList.add(row);
			}
			String[][] array2d = new String[arrayList.size()][columnCount];
			for (int i = 0; i < arrayList.size(); i++) {
				array2d[i] = arrayList.get(i);
			}
			return array2d;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	public String[][] getData(String table, int branch) throws Exception {
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + table + " WHERE Chi_nhanh=" + branch);
			
			ResultSet result = statement.executeQuery();
			ResultSetMetaData metaData = result.getMetaData();
			int columnCount = metaData.getColumnCount(); //number of column
			ArrayList<String[]> arrayList = new ArrayList<String[]>();
			while(result.next()) {
				String[] row = new String[columnCount];
				for (int i = 1; i <= columnCount; i++) {
					row[i-1] = String.valueOf(result.getObject(i));
				}
				arrayList.add(row);
			}
			String[][] array2d = new String[arrayList.size()][columnCount];
			for (int i = 0; i < arrayList.size(); i++) {
				array2d[i] = arrayList.get(i);
			}
			return array2d;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	
	
	public void insert(String table, String[] values) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			String sql = "INSERT INTO " + DATABASE_NAME + "." + table + " VALUES (";
			for (int i = 0; i < values.length; i++) {
				if (i == values.length - 1) {
					sql += "'" + values[i] + "');"; 
				} else {
					sql += "'" + values[i] + "',"; 
				}
			}
			System.out.println(sql);
			statement.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void update(String table, String[] titles, String[] values) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			String sql = "UPDATE " + DATABASE_NAME + "." + table + " SET ";
			for (int i = 1; i < titles.length; i++) {
				if (i == titles.length - 1) {
					sql += titles[i] + "=" + "'" + values[i] + "' "; 
				} else {
					sql += titles[i] + "=" + "'" + values[i] + "', "; 
				}
			}
			sql += "WHERE " + titles[0] + "=" + "'" + values[0] + "'";
			System.out.println(sql);
			statement.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void delete(String table, String key, String value) throws Exception {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			String sql = "DELETE FROM " + table + " WHERE " + key + "=" + "'" + value + "'";
			System.out.println(sql);
			statement.executeUpdate(sql);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
