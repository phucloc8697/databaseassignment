package database;

import utility.Constant;

public class DatabaseActivity extends DatabaseHelper {

	public String[] getActivityNames() {
		String[] array = {
				Constant.TABLE_CHO_AN_AO,
				Constant.TABLE_DIEU_TRI_THUOC,
				Constant.TABLE_LAN_CHO_AN,
				Constant.TABLE_LAN_THA,
				Constant.TABLE_THA_TOM_AO,
				Constant.TABLE_VAN_CHUYEN};
		return array;
	}
	
}
