import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import database.DatabaseHelper;

public class BuyDialog extends JDialog implements ActionListener {

	public interface Listener {
		public void onReturnValue(Object obj);
	}
		
	private final JPanel contentPanel = new JPanel();
	private JScrollPane scrollPane;
	private JButton okButton;
	private JButton cancelButton;
	private JTextField[] textFields;
	
	private Listener listener;
	private String[] xeTitles;
	private String[] billTitles;
	private int currentId;

	private static final Insets WEST_INSETS = new Insets(5, 10, 5, 5);
	private static final Insets EAST_INSETS = new Insets(5, 5, 5, 10);
	private static final int MAX_FORM_HEIGHT = 400;
	   
	public BuyDialog(JFrame owner, boolean modal, int id) throws Exception {
		super(owner, modal);
		
		this.currentId = id;
		String[] xeArray = {"Ten xe", "Gia mua", "Loai xe", "Mau", "Bien so", "Chi nhanh"};
		this.xeTitles = xeArray;
		
		String[] billArray = {"Tong gia", "Ngay giao", "Ngay thanh toan", "Hinh thuc thanh toan", "Doi tac"};
		this.billTitles = billArray;
		this.textFields = new JTextField[xeTitles.length + billTitles.length];
		
		setBounds(150, 150, 400, textFields.length > 4 ? MAX_FORM_HEIGHT : textFields.length * 80);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);		
		contentPanel.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		contentPanel.add(scrollPane);
		
		addForm();
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		okButton = new JButton("SAVE");
		okButton.setActionCommand("Save");
		okButton.addActionListener(this);
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	
		cancelButton = new JButton("CANCEL");
		cancelButton.setActionCommand("Cancel");
		cancelButton.addActionListener(this);
		buttonPane.add(cancelButton);
	}
	
	private void addForm() {
		JPanel pane = new JPanel(new GridBagLayout());
		scrollPane.setViewportView(pane);
		
	    for (int i = 0; i < xeTitles.length; i++) {
	        JLabel label = new JLabel(xeTitles[i]);
	        pane.add(label, createGbc(0, i));
	        
	        textFields[i] = new JTextField();
	        pane.add(textFields[i], createGbc(1, i));
	    }
	    
	    for (int i = xeTitles.length; i < textFields.length; i++) {
	        JLabel label = new JLabel(billTitles[i - xeTitles.length]);
	        pane.add(label, createGbc(0, i));
	        
	        textFields[i] = new JTextField();
	        pane.add(textFields[i], createGbc(1, i));
	    }
	}
	
	public void addListener(Listener listener) {
		this.listener = listener;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Save":
			insert();
			break;
		case "Cancel":
			dispose();
			break;
		default:
			break;
		}
	}
	
	private void insert() {
		String[] xeValues = new String[xeTitles.length];
		String[] billValues = new String[billTitles.length];
		for (int i = 0; i < xeTitles.length; i++) {
			if (textFields[i].getText().toString().isEmpty()) {
				JOptionPane.showMessageDialog(null, "You must fill all text field to insert!");
				return;
			} else {
				xeValues[i] = textFields[i].getText();
			}
		}
		for (int i = 0; i < billTitles.length; i++) {
			if (textFields[i + xeTitles.length].getText().toString().isEmpty()) {
				JOptionPane.showMessageDialog(null, "You must fill all text field to insert!");
				return;
			} else {
				billValues[i] = textFields[i + xeTitles.length].getText();
			}
		}
		try {
			DatabaseHelper.bill.insertMuaXe(xeValues, billValues, this.currentId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.listener.onReturnValue(null);
		dispose();
	}
	
	private GridBagConstraints createGbc(int x, int y) {
	      GridBagConstraints gbc = new GridBagConstraints();
	      gbc.gridx = x;
	      gbc.gridy = y;
	      gbc.gridwidth = 1;
	      gbc.gridheight = 1;

	      gbc.anchor = (x == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
	      gbc.fill = (x == 0) ? GridBagConstraints.BOTH
	            : GridBagConstraints.HORIZONTAL;

	      gbc.insets = (x == 0) ? WEST_INSETS : EAST_INSETS;
	      gbc.weightx = (x == 0) ? 0.1 : 1.0;
	      gbc.weighty = 1.0;
	      return gbc;
	   }
}
