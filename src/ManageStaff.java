import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import database.DatabaseHelper;
import utility.Constant;

public class ManageStaff extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton btnInsert;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JButton backButton;
	private JTable table;
		
	private int currentId;
	private String role;
	private int branch;
	
	public ManageStaff(int id) {
		this.currentId = id;
		this.role = DatabaseHelper.shared.getRole(id);
		this.branch = DatabaseHelper.staff.getBranch(id);
		try {
			initialize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initialize() throws Exception {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnInsert = new JButton("Th\u00EAm nh\u00E2n vi\u00EAn");
		btnInsert.addActionListener(this);
		btnInsert.setActionCommand("Insert");
		btnInsert.setBounds(10, 11, 190, 38);
		contentPane.add(btnInsert);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(210, 11, 764, 389);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setBounds(314, 11, 660, 389);
		scrollPane.setViewportView(table);
		
		if (this.role.equals(Constant.ROLL_MANAGER) || this.role.equals(Constant.ROLL_BOSS)) {
			btnUpdate = new JButton("Ch\u1EC9nh s\u1EEDa nh\u00E2n vi\u00EAn");
			btnUpdate.setBounds(10, 60, 190, 38);
			btnUpdate.setActionCommand("Update");
			btnUpdate.addActionListener(this);
			contentPane.add(btnUpdate);
			
			btnDelete = new JButton("X\u00F3a nh\u00E2n vi\u00EAn");
			btnDelete.setBounds(10, 109, 190, 38);
			btnDelete.setActionCommand("Delete");
			btnDelete.addActionListener(this);
			contentPane.add(btnDelete);
		}
		
		backButton = new JButton("Quay l\u1EA1i");
		backButton.setBounds(10, 362, 190, 38);
		backButton.setActionCommand("Back");
		backButton.addActionListener(this);
		contentPane.add(backButton);
		
		updateTable();
	}
	
	private void updateTable() throws Exception {
		String[] titles = DatabaseHelper.staff.getTitles(this.branch);
		String[][] data = DatabaseHelper.staff.getData(this.branch);
		table.setModel(new DefaultTableModel(data, titles));
	}
	
	private void insert() {
		try {
			StaffDialog dialog = new StaffDialog(this, true, null);
			dialog.addListener(new StaffDialog.Listener() {
				
				@Override
				public void onReturnValue(Object obj) {
					try {
						updateTable();
					} catch (Exception e1) {
						System.out.println(e1);
					}
				}
			});
			dialog.setVisible(true);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void update() {
		try {
			int index = table.getSelectedRow();
			if (index == -1) {
				JOptionPane.showMessageDialog(null, "You must choose a row to update");
				return;
			}
			
			String[] titles = DatabaseHelper.shared.getTitles(Constant.VIEW_NHAN_VIEN_1);
			String[] values = new String[titles.length];
			for (int i = 0; i < titles.length; i++) {
				values[i] = (String) table.getValueAt(index, i);
			}
			StaffDialog dialog = new StaffDialog(this, true, values);
			dialog.addListener(new StaffDialog.Listener() {
				
				@Override
				public void onReturnValue(Object obj) {
					try {
						updateTable();
					} catch (Exception e1) {
						System.out.println(e1);
					}
				}
			});
			dialog.setVisible(true);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void delete() {
		int index = table.getSelectedRow();
		if (index == -1) {
			JOptionPane.showMessageDialog(null, "You must choose a row to delete");
			return;
		}
		String key = table.getColumnName(0);
		try {
			DatabaseHelper.shared.delete(
					Constant.TABLE_NHAN_VIEN,
					key,
					(String) table.getValueAt(index, 0));
			updateTable();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void back() {
		Menu frame = new Menu(this.currentId);
		frame.setVisible(true);
		dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Insert":
			insert();
			break;
		case "Update":
			update();
			break;
		case "Delete":
			delete();
			break;
		case "Back":
			back();
			break;
		default:
			break;
		}
	}
}