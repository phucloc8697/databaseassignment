import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import database.DatabaseHelper;
import utility.Constant;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

public class Home extends JFrame implements ActionListener {
	
	private JPanel contentPane;
	private JComboBox<String> comboBox;
	private JButton btnInsert;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JButton btnSignOut;
	private JTable table;
	
	private String role;
	
	public Home(String role) {
		this.role = role;
		try {
			initialize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initialize() throws Exception {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
				
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(DatabaseHelper.shared.getAllTableNames(this.role)));
		comboBox.setBounds(10, 11, 294, 38);
		comboBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				try {
					updateTable();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(comboBox);
		
		btnInsert = new JButton("INSERT");
		btnInsert.addActionListener(this);
		btnInsert.setActionCommand("Insert");
		btnInsert.setBounds(10, 80, 294, 38);
		contentPane.add(btnInsert);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(314, 11, 660, 389);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setBounds(314, 11, 660, 389);
		scrollPane.setViewportView(table);
		
		if (this.role.equals(Constant.ROLL_MANAGER) || this.role.equals(Constant.ROLL_BOSS)) {
			btnUpdate = new JButton("UPDATE");
			btnUpdate.setBounds(10, 129, 294, 38);
			btnUpdate.setActionCommand("Update");
			btnUpdate.addActionListener(this);
			contentPane.add(btnUpdate);
			
			btnDelete = new JButton("DELETE");
			btnDelete.setBounds(10, 178, 294, 38);
			btnDelete.setActionCommand("Delete");
			btnDelete.addActionListener(this);
			contentPane.add(btnDelete);
		}
		
		btnSignOut = new JButton("SIGN OUT");
		btnSignOut.setBounds(10, 362, 294, 38);
		btnSignOut.setActionCommand("Sign out");
		btnSignOut.addActionListener(this);
		contentPane.add(btnSignOut);
		
		updateTable();
	}
	
	private void updateTable() throws Exception {
		String selectTable = (String) comboBox.getSelectedItem();
		String[] titles = DatabaseHelper.shared.getTitles(selectTable);
		String[][] data = DatabaseHelper.shared.getData(selectTable);
		table.setModel(new DefaultTableModel(data, titles));
	}
	
	private void insert() {
		try {
			String[] titles = DatabaseHelper.shared.getTitles((String) comboBox.getSelectedItem());
			InsertDialog dialog = new InsertDialog(this, true, titles);
			dialog.addListener(new InsertDialog.Listener() {
				
				@Override
				public void onReturnValue(Object obj) {
					String[] values = (String[]) obj;
					try {
						DatabaseHelper.shared.insert((String) comboBox.getSelectedItem(), values);
						updateTable();
					} catch (Exception e) {
						System.out.println(e);
					}
				}
			});
			dialog.setVisible(true);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void update() {
		try {
			int index = table.getSelectedRow();
			if (index == -1) {
				JOptionPane.showMessageDialog(null, "You must choose a row to delete");
				return;
			}
			
			String[] titles = DatabaseHelper.shared.getTitles((String) comboBox.getSelectedItem());
			String[] values = new String[titles.length];
			for (int i = 0; i < titles.length; i++) {
				values[i] = (String) table.getValueAt(index, i);
			}
			UpdateDialog dialog = new UpdateDialog(this, true, titles, values);
			dialog.addListener(new UpdateDialog.Listener() {
				
				@Override
				public void onReturnValue(Object obj) {
					String[] values = (String[]) obj;
					try {
						DatabaseHelper.shared.update((String) comboBox.getSelectedItem(), titles, values);
						updateTable();
					} catch (Exception e) {
						System.out.println(e);
					}
				}
			});
			dialog.setVisible(true);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void delete() {
		int index = table.getSelectedRow();
		if (index == -1) {
			JOptionPane.showMessageDialog(null, "You must choose a row to delete");
			return;
		}
		String key = table.getColumnName(0);
		try {
			DatabaseHelper.shared.delete(
					(String) comboBox.getSelectedItem(),
					key,
					(String) table.getValueAt(index, 0));
			updateTable();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private void signOut() {
		Login frame = new Login();
		frame.setVisible(true);
		dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Insert":
			insert();
			break;
		case "Update":
			update();
			break;
		case "Delete":
			delete();
			break;
		case "Sign out":
			signOut();
			break;
		default:
			break;
		}
	}
}
