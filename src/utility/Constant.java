package utility;

public class Constant {
	
	public static final String TABLE_NGUOI_DUNG = "nguoi_dung";
	public static final String TABLE_NHAN_VIEN = "nhan_vien";
	public static final String VIEW_NHAN_VIEN_1 = "nhan_vien1";
	public static final String VIEW_NHAN_VIEN_2 = "nhan_vien2";
	public static final String VIEW_NHAN_VIEN_3 = "nhan_vien3";
	public static final String TABLE_LAI_XE = "lai_xe";
	
	public static final String TABLE_CHO_AN_AO = "cho_an_ao";
	public static final String TABLE_DIEU_TRI_THUOC = "dieu_tri_thuoc";
	public static final String TABLE_LAN_CHO_AN = "lan_cho_an";
	public static final String TABLE_LAN_THA = "lan_tha";
	public static final String TABLE_THA_TOM_AO = "tha_tom_ao";
	public static final String TABLE_VAN_CHUYEN = "van_chuyen";
	
	public static final String TABLE_HOA_DON = "hoa_don";
	public static final String TABLE_HOA_DON_MUA = "hoa_don_mua";
	public static final String TABLE_HOA_DON_BAN = "hoa_don_ban";
	public static final String VIEW_HOA_DON_MUA_1 = "hoa_don_mua1";
	public static final String VIEW_HOA_DON_MUA_2 = "hoa_don_mua2";
	public static final String VIEW_HOA_DON_MUA_3 = "hoa_don_mua3";
	public static final String VIEW_HOA_DON_BAN_1 = "hoa_don_ban1";
	public static final String VIEW_HOA_DON_BAN_2 = "hoa_don_ban2";
	public static final String VIEW_HOA_DON_BAN_3 = "hoa_don_ban3";

	public static final String TABLE_LO_THUC_AN = "lo_thuc_an";
	public static final String TABLE_LO_TOM = "lo_tom";
	public static final String TABLE_THUOC = "thuoc";
	public static final String TABLE_VAT_TU = "vat_tu";
	public static final String TABLE_XE = "xe";

	public static final String ROLL_STAFF = "staff";
	public static final String ROLL_MANAGER = "manager";
	public static final String ROLL_BOSS = "boss";
}
