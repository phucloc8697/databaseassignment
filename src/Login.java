import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import database.DatabaseHelper;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;

public class Login extends JFrame implements KeyListener {

	private JPanel contentPane;
	private JTextField userNameTextField;
	private JTextField passwordTextField;
	private JButton loginButton;

	public enum Role {
		MANAGER,
		STAFF
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsername.setBounds(10, 11, 100, 20);
		contentPane.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(10, 42, 100, 20);
		contentPane.add(lblPassword);
		
		userNameTextField = new JTextField();
		userNameTextField.setBounds(120, 13, 204, 20);
		userNameTextField.setColumns(10);
		userNameTextField.addKeyListener(this);
		contentPane.add(userNameTextField);
		
		passwordTextField = new JPasswordField();
		passwordTextField.setColumns(10);
		passwordTextField.setBounds(120, 44, 204, 20);
		passwordTextField.addKeyListener(this);
		contentPane.add(passwordTextField);
		
		JButton loginButton = new JButton("LOGIN");
		loginButton.setBounds(235, 127, 89, 23);
		loginButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				login();
			}
		});
		contentPane.add(loginButton);
	}
	
	private void login() {
		int id = DatabaseHelper.shared.authenticate(
				userNameTextField.getText(),
				passwordTextField.getText()
		);
		System.out.println(id);
		if (id == 0) {
			JOptionPane.showMessageDialog(null, "Invalid username / password");
		} else {
			Menu frame = new Menu(id);
			frame.setVisible(true);
			dispose();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			login();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}
}
